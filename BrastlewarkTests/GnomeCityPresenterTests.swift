//
//  GnomeCityPresenterTests.swift
//  BrastlewarkTests
//
//  Created by Anton Zuev on 11/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import XCTest
import Brastlewark

class GnomeCityPresenterTests: XCTestCase {
    
    var presenter: GnomeCityPresenter!
    var emptyGnome:Gnome = Gnome()
    var view: MockAllGnomesViewProtocol!
    
    override func setUp() {
        super.setUp()
        self.view = MockAllGnomesViewProtocol()
        self.presenter = GnomeCityPresenter(view: self.view)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNumberOfCitizens() {
        //set the model of the presenter
        self.presenter.city.habitantsInfo = Brastlewark(Brastlewark: [Gnome(),Gnome(),Gnome(),Gnome()])
        XCTAssert(4 == presenter.getNumberOfCitizens(), "The number of citizens is incorrect")
    }
    
    func testGetInfoAboutHabitant() {
        //set the model of the presenter
        let firstGnome = Gnome()
        firstGnome.name = "Lazy Bean"
        let secondGnome = Gnome()
        secondGnome.name = "Crazy Law"
        let thirdGnome = Gnome()
        thirdGnome.name = "Sleazy"
        self.presenter.city.habitantsInfo = Brastlewark(Brastlewark: [firstGnome,secondGnome,thirdGnome])
        XCTAssert(firstGnome.name == self.presenter.getInfoAboutHabitant(index: 0)?.name, "The name of first habitant is incorrect")
        XCTAssert(secondGnome.name == self.presenter.getInfoAboutHabitant(index: 1)?.name, "The name of second habitant is incorrect")
        XCTAssert(thirdGnome.name == self.presenter.getInfoAboutHabitant(index: 2)?.name, "The name of third habitant is incorrect")
    }
    
}

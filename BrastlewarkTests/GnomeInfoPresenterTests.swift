//
//  GnomeInfoPresenterTests.swift
//  BrastlewarkTests
//
//  Created by Anton Zuev on 11/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import XCTest
import Brastlewark
import Cuckoo

class GnomeInfoPresenterTests: XCTestCase {
    
    var presenter: GnomeInfoPresenter!
    var emptyGnome:Gnome = Gnome()
    var view: MockGnomeDetailsProtocol!
    
    override func setUp() {
        super.setUp()
        self.view = MockGnomeDetailsProtocol()
        self.presenter = GnomeInfoPresenter(view: self.view)
        stub(self.view){
            when($0.setTitle(title:anyString())).thenDoNothing()
            when($0.setImage()).thenDoNothing()
        }
    }
    ///Define this method for let Cuckoo work with optional String object
    func equal(to value: String?) -> ParameterMatcher<String?> {
        return ParameterMatcher { tested in
            // Implementation of your equality test.
            (tested == value)
        }
    }
    
    func testNumberOfRowsOfEmptyModelObject() {
        //set the empty model
        self.presenter.setGnomeInfo(gnomeInfo: emptyGnome)
        XCTAssert(4 == self.presenter.getNumberOfRows(), "This is not full information of empty model")
    }
    
    func testNumberOfRowsOfModelObjectWithFriends() {
        //set the model
        let gnome = Gnome()
        gnome.friends = ["First friend", "Second friend"]
        self.presenter.setGnomeInfo(gnomeInfo: gnome)
        XCTAssert(5 == self.presenter.getNumberOfRows(), "This is not full information of model.")
    }
    
    func testNumberOfRowsOfFullModelObjectWithProfessions() {
        //set the model
        let gnome = Gnome()
        gnome.friends = ["First friend", "Second friend"]
        gnome.professions = ["First profession", "Second profession"]
        self.presenter.setGnomeInfo(gnomeInfo: gnome)
        XCTAssert(6 == self.presenter.getNumberOfRows(), "This is not full information of model.")
    }
    
    ///Check if setters of views are called
    func testSettingViews() {
        //set the model
        let gnome = Gnome()
        gnome.name = "Name"
        gnome.friends = ["First friend", "Second friend"]
        gnome.professions = ["First profession", "Second profession"]
        self.presenter.setGnomeInfo(gnomeInfo: gnome)
        self.presenter.showGnomeImageAndName()
        verify(self.view).setImage()
        verify(self.view).setTitle(title: self.equal(to: gnome.name))
    }
    
    
}

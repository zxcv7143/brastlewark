// MARK: - Mocks generated from file: Brastlewark/View/AllGnomesViewController.swift at 2018-06-13 17:07:17 +0000

//
//  ViewController.swift
//  Brastlewark
//
//  Created by Anton Zuev on 7/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Cuckoo
@testable import Brastlewark

import UIKit

class MockAllGnomesViewProtocol: AllGnomesViewProtocol, Cuckoo.ProtocolMock {
    typealias MocksType = AllGnomesViewProtocol
    typealias Stubbing = __StubbingProxy_AllGnomesViewProtocol
    typealias Verification = __VerificationProxy_AllGnomesViewProtocol
    let cuckoo_manager = Cuckoo.MockManager(hasParent: false)

    

    

    
    // ["name": "reloadView", "returnSignature": "", "fullyQualifiedName": "reloadView()", "parameterSignature": "", "parameterSignatureWithoutNames": "", "inputTypes": "", "isThrowing": false, "isInit": false, "isOverriding": false, "hasClosureParams": false, "@type": "ProtocolMethod", "accessibility": "", "parameterNames": "", "call": "", "parameters": [], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ProtocolStubNoReturnFunction"]
     func reloadView()  {
        
            return cuckoo_manager.call("reloadView()",
                parameters: (),
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    )
        
    }
    

	struct __StubbingProxy_AllGnomesViewProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	    init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func reloadView() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockAllGnomesViewProtocol.self, method: "reloadView()", parameterMatchers: matchers))
	    }
	    
	}

	struct __VerificationProxy_AllGnomesViewProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	    init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func reloadView() -> Cuckoo.__DoNotUse<Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("reloadView()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}

}

 class AllGnomesViewProtocolStub: AllGnomesViewProtocol {
    

    

    
     func reloadView()  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
}


class MockAllGnomesViewController: AllGnomesViewController, Cuckoo.ClassMock {
    typealias MocksType = AllGnomesViewController
    typealias Stubbing = __StubbingProxy_AllGnomesViewController
    typealias Verification = __VerificationProxy_AllGnomesViewController
    let cuckoo_manager = Cuckoo.MockManager(hasParent: true)

    
    // ["name": "presenter", "stubType": "ClassToBeStubbedProperty", "@type": "InstanceVariable", "type": "GnomeCityPresenter?", "isReadOnly": false, "accessibility": ""]
     override var presenter: GnomeCityPresenter? {
        get {
            
            return cuckoo_manager.getter("presenter", superclassCall: super.presenter)
            
        }
        
        set {
            
            cuckoo_manager.setter("presenter", value: newValue, superclassCall: super.presenter = newValue)
            
        }
        
    }
    
    // ["name": "collectionView", "stubType": "ClassToBeStubbedProperty", "@type": "InstanceVariable", "type": "UICollectionView!", "isReadOnly": false, "accessibility": ""]
     override var collectionView: UICollectionView! {
        get {
            
            return cuckoo_manager.getter("collectionView", superclassCall: super.collectionView)
            
        }
        
        set {
            
            cuckoo_manager.setter("collectionView", value: newValue, superclassCall: super.collectionView = newValue)
            
        }
        
    }
    

    

    
    // ["name": "awakeFromNib", "returnSignature": "", "fullyQualifiedName": "awakeFromNib()", "parameterSignature": "", "parameterSignatureWithoutNames": "", "inputTypes": "", "isThrowing": false, "isInit": false, "isOverriding": true, "hasClosureParams": false, "@type": "ClassMethod", "accessibility": "", "parameterNames": "", "call": "", "parameters": [], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ClassStubNoReturnFunction"]
     override func awakeFromNib()  {
        
            return cuckoo_manager.call("awakeFromNib()",
                parameters: (),
                superclassCall:
                    
                    super.awakeFromNib()
                    )
        
    }
    
    // ["name": "viewDidLoad", "returnSignature": "", "fullyQualifiedName": "viewDidLoad()", "parameterSignature": "", "parameterSignatureWithoutNames": "", "inputTypes": "", "isThrowing": false, "isInit": false, "isOverriding": true, "hasClosureParams": false, "@type": "ClassMethod", "accessibility": "", "parameterNames": "", "call": "", "parameters": [], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ClassStubNoReturnFunction"]
     override func viewDidLoad()  {
        
            return cuckoo_manager.call("viewDidLoad()",
                parameters: (),
                superclassCall:
                    
                    super.viewDidLoad()
                    )
        
    }
    
    // ["name": "didReceiveMemoryWarning", "returnSignature": "", "fullyQualifiedName": "didReceiveMemoryWarning()", "parameterSignature": "", "parameterSignatureWithoutNames": "", "inputTypes": "", "isThrowing": false, "isInit": false, "isOverriding": true, "hasClosureParams": false, "@type": "ClassMethod", "accessibility": "", "parameterNames": "", "call": "", "parameters": [], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ClassStubNoReturnFunction"]
     override func didReceiveMemoryWarning()  {
        
            return cuckoo_manager.call("didReceiveMemoryWarning()",
                parameters: (),
                superclassCall:
                    
                    super.didReceiveMemoryWarning()
                    )
        
    }
    
    // ["name": "reloadView", "returnSignature": "", "fullyQualifiedName": "reloadView()", "parameterSignature": "", "parameterSignatureWithoutNames": "", "inputTypes": "", "isThrowing": false, "isInit": false, "isOverriding": true, "hasClosureParams": false, "@type": "ClassMethod", "accessibility": "", "parameterNames": "", "call": "", "parameters": [], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ClassStubNoReturnFunction"]
     override func reloadView()  {
        
            return cuckoo_manager.call("reloadView()",
                parameters: (),
                superclassCall:
                    
                    super.reloadView()
                    )
        
    }
    
    // ["name": "prepare", "returnSignature": "", "fullyQualifiedName": "prepare(for: UIStoryboardSegue, sender: Any?)", "parameterSignature": "for segue: UIStoryboardSegue, sender: Any?", "parameterSignatureWithoutNames": "segue: UIStoryboardSegue, sender: Any?", "inputTypes": "UIStoryboardSegue, Any?", "isThrowing": false, "isInit": false, "isOverriding": true, "hasClosureParams": false, "@type": "ClassMethod", "accessibility": "", "parameterNames": "segue, sender", "call": "for: segue, sender: sender", "parameters": [CuckooGeneratorFramework.MethodParameter(label: Optional("for"), name: "segue", type: "UIStoryboardSegue", range: CountableRange(1153..<1181), nameRange: CountableRange(1153..<1156)), CuckooGeneratorFramework.MethodParameter(label: Optional("sender"), name: "sender", type: "Any?", range: CountableRange(1183..<1195), nameRange: CountableRange(1183..<1189))], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ClassStubNoReturnFunction"]
     override func prepare(for segue: UIStoryboardSegue, sender: Any?)  {
        
            return cuckoo_manager.call("prepare(for: UIStoryboardSegue, sender: Any?)",
                parameters: (segue, sender),
                superclassCall:
                    
                    super.prepare(for: segue, sender: sender)
                    )
        
    }
    

	struct __StubbingProxy_AllGnomesViewController: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	    init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    var presenter: Cuckoo.ClassToBeStubbedProperty<MockAllGnomesViewController, GnomeCityPresenter?> {
	        return .init(manager: cuckoo_manager, name: "presenter")
	    }
	    
	    var collectionView: Cuckoo.ClassToBeStubbedProperty<MockAllGnomesViewController, UICollectionView?> {
	        return .init(manager: cuckoo_manager, name: "collectionView")
	    }
	    
	    
	    func awakeFromNib() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockAllGnomesViewController.self, method: "awakeFromNib()", parameterMatchers: matchers))
	    }
	    
	    func viewDidLoad() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockAllGnomesViewController.self, method: "viewDidLoad()", parameterMatchers: matchers))
	    }
	    
	    func didReceiveMemoryWarning() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockAllGnomesViewController.self, method: "didReceiveMemoryWarning()", parameterMatchers: matchers))
	    }
	    
	    func reloadView() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockAllGnomesViewController.self, method: "reloadView()", parameterMatchers: matchers))
	    }
	    
	    func prepare<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(for segue: M1, sender: M2) -> Cuckoo.ClassStubNoReturnFunction<(UIStoryboardSegue, Any?)> where M1.MatchedType == UIStoryboardSegue, M2.MatchedType == Any? {
	        let matchers: [Cuckoo.ParameterMatcher<(UIStoryboardSegue, Any?)>] = [wrap(matchable: segue) { $0.0 }, wrap(matchable: sender) { $0.1 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockAllGnomesViewController.self, method: "prepare(for: UIStoryboardSegue, sender: Any?)", parameterMatchers: matchers))
	    }
	    
	}

	struct __VerificationProxy_AllGnomesViewController: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	    init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    var presenter: Cuckoo.VerifyProperty<GnomeCityPresenter?> {
	        return .init(manager: cuckoo_manager, name: "presenter", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    var collectionView: Cuckoo.VerifyProperty<UICollectionView?> {
	        return .init(manager: cuckoo_manager, name: "collectionView", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func awakeFromNib() -> Cuckoo.__DoNotUse<Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("awakeFromNib()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func viewDidLoad() -> Cuckoo.__DoNotUse<Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("viewDidLoad()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func didReceiveMemoryWarning() -> Cuckoo.__DoNotUse<Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("didReceiveMemoryWarning()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func reloadView() -> Cuckoo.__DoNotUse<Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("reloadView()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func prepare<M1: Cuckoo.Matchable, M2: Cuckoo.Matchable>(for segue: M1, sender: M2) -> Cuckoo.__DoNotUse<Void> where M1.MatchedType == UIStoryboardSegue, M2.MatchedType == Any? {
	        let matchers: [Cuckoo.ParameterMatcher<(UIStoryboardSegue, Any?)>] = [wrap(matchable: segue) { $0.0 }, wrap(matchable: sender) { $0.1 }]
	        return cuckoo_manager.verify("prepare(for: UIStoryboardSegue, sender: Any?)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}

}

 class AllGnomesViewControllerStub: AllGnomesViewController {
    
     override var presenter: GnomeCityPresenter? {
        get {
            return DefaultValueRegistry.defaultValue(for: (GnomeCityPresenter?).self)
        }
        
        set { }
        
    }
    
     override var collectionView: UICollectionView! {
        get {
            return DefaultValueRegistry.defaultValue(for: (UICollectionView!).self)
        }
        
        set { }
        
    }
    

    

    
     override func awakeFromNib()  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
     override func viewDidLoad()  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
     override func didReceiveMemoryWarning()  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
     override func reloadView()  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?)  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
}


// MARK: - Mocks generated from file: Brastlewark/View/GnomeDetailsViewController.swift at 2018-06-13 17:07:17 +0000

//
//  GnomeInfoViewController.swift
//  Brastlewark
//
//  Created by Anton Zuev on 8/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Cuckoo
@testable import Brastlewark

import UIKit

class MockGnomeDetailsProtocol: GnomeDetailsProtocol, Cuckoo.ProtocolMock {
    typealias MocksType = GnomeDetailsProtocol
    typealias Stubbing = __StubbingProxy_GnomeDetailsProtocol
    typealias Verification = __VerificationProxy_GnomeDetailsProtocol
    let cuckoo_manager = Cuckoo.MockManager(hasParent: false)

    

    

    
    // ["name": "setTitle", "returnSignature": "", "fullyQualifiedName": "setTitle(title: String?)", "parameterSignature": "title: String?", "parameterSignatureWithoutNames": "title: String?", "inputTypes": "String?", "isThrowing": false, "isInit": false, "isOverriding": false, "hasClosureParams": false, "@type": "ProtocolMethod", "accessibility": "", "parameterNames": "title", "call": "title: title", "parameters": [CuckooGeneratorFramework.MethodParameter(label: Optional("title"), name: "title", type: "String?", range: CountableRange(221..<235), nameRange: CountableRange(221..<226))], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ProtocolStubNoReturnFunction"]
     func setTitle(title: String?)  {
        
            return cuckoo_manager.call("setTitle(title: String?)",
                parameters: (title),
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    )
        
    }
    
    // ["name": "setImage", "returnSignature": "", "fullyQualifiedName": "setImage()", "parameterSignature": "", "parameterSignatureWithoutNames": "", "inputTypes": "", "isThrowing": false, "isInit": false, "isOverriding": false, "hasClosureParams": false, "@type": "ProtocolMethod", "accessibility": "", "parameterNames": "", "call": "", "parameters": [], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ProtocolStubNoReturnFunction"]
     func setImage()  {
        
            return cuckoo_manager.call("setImage()",
                parameters: (),
                superclassCall:
                    
                    Cuckoo.MockManager.crashOnProtocolSuperclassCall()
                    )
        
    }
    

	struct __StubbingProxy_GnomeDetailsProtocol: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	    init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    
	    func setTitle<M1: Cuckoo.Matchable>(title: M1) -> Cuckoo.ProtocolStubNoReturnFunction<(String?)> where M1.MatchedType == String? {
	        let matchers: [Cuckoo.ParameterMatcher<(String?)>] = [wrap(matchable: title) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockGnomeDetailsProtocol.self, method: "setTitle(title: String?)", parameterMatchers: matchers))
	    }
	    
	    func setImage() -> Cuckoo.ProtocolStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockGnomeDetailsProtocol.self, method: "setImage()", parameterMatchers: matchers))
	    }
	    
	}

	struct __VerificationProxy_GnomeDetailsProtocol: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	    init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	
	    
	    @discardableResult
	    func setTitle<M1: Cuckoo.Matchable>(title: M1) -> Cuckoo.__DoNotUse<Void> where M1.MatchedType == String? {
	        let matchers: [Cuckoo.ParameterMatcher<(String?)>] = [wrap(matchable: title) { $0 }]
	        return cuckoo_manager.verify("setTitle(title: String?)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func setImage() -> Cuckoo.__DoNotUse<Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("setImage()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}

}

 class GnomeDetailsProtocolStub: GnomeDetailsProtocol {
    

    

    
     func setTitle(title: String?)  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
     func setImage()  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
}


class MockGnomeDetailsViewController: GnomeDetailsViewController, Cuckoo.ClassMock {
    typealias MocksType = GnomeDetailsViewController
    typealias Stubbing = __StubbingProxy_GnomeDetailsViewController
    typealias Verification = __VerificationProxy_GnomeDetailsViewController
    let cuckoo_manager = Cuckoo.MockManager(hasParent: true)

    
    // ["name": "gnomePhotoImageView", "stubType": "ClassToBeStubbedProperty", "@type": "InstanceVariable", "type": "UIImageView!", "isReadOnly": false, "accessibility": ""]
     override var gnomePhotoImageView: UIImageView! {
        get {
            
            return cuckoo_manager.getter("gnomePhotoImageView", superclassCall: super.gnomePhotoImageView)
            
        }
        
        set {
            
            cuckoo_manager.setter("gnomePhotoImageView", value: newValue, superclassCall: super.gnomePhotoImageView = newValue)
            
        }
        
    }
    
    // ["name": "infoTableView", "stubType": "ClassToBeStubbedProperty", "@type": "InstanceVariable", "type": "UITableView!", "isReadOnly": false, "accessibility": ""]
     override var infoTableView: UITableView! {
        get {
            
            return cuckoo_manager.getter("infoTableView", superclassCall: super.infoTableView)
            
        }
        
        set {
            
            cuckoo_manager.setter("infoTableView", value: newValue, superclassCall: super.infoTableView = newValue)
            
        }
        
    }
    
    // ["name": "presenter", "stubType": "ClassToBeStubbedProperty", "@type": "InstanceVariable", "type": "GnomeInfoPresenter?", "isReadOnly": false, "accessibility": ""]
     override var presenter: GnomeInfoPresenter? {
        get {
            
            return cuckoo_manager.getter("presenter", superclassCall: super.presenter)
            
        }
        
        set {
            
            cuckoo_manager.setter("presenter", value: newValue, superclassCall: super.presenter = newValue)
            
        }
        
    }
    

    

    
    // ["name": "viewDidLoad", "returnSignature": "", "fullyQualifiedName": "viewDidLoad()", "parameterSignature": "", "parameterSignatureWithoutNames": "", "inputTypes": "", "isThrowing": false, "isInit": false, "isOverriding": true, "hasClosureParams": false, "@type": "ClassMethod", "accessibility": "", "parameterNames": "", "call": "", "parameters": [], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ClassStubNoReturnFunction"]
     override func viewDidLoad()  {
        
            return cuckoo_manager.call("viewDidLoad()",
                parameters: (),
                superclassCall:
                    
                    super.viewDidLoad()
                    )
        
    }
    
    // ["name": "didReceiveMemoryWarning", "returnSignature": "", "fullyQualifiedName": "didReceiveMemoryWarning()", "parameterSignature": "", "parameterSignatureWithoutNames": "", "inputTypes": "", "isThrowing": false, "isInit": false, "isOverriding": true, "hasClosureParams": false, "@type": "ClassMethod", "accessibility": "", "parameterNames": "", "call": "", "parameters": [], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ClassStubNoReturnFunction"]
     override func didReceiveMemoryWarning()  {
        
            return cuckoo_manager.call("didReceiveMemoryWarning()",
                parameters: (),
                superclassCall:
                    
                    super.didReceiveMemoryWarning()
                    )
        
    }
    
    // ["name": "setTitle", "returnSignature": "", "fullyQualifiedName": "setTitle(title: String?)", "parameterSignature": "title: String?", "parameterSignatureWithoutNames": "title: String?", "inputTypes": "String?", "isThrowing": false, "isInit": false, "isOverriding": true, "hasClosureParams": false, "@type": "ClassMethod", "accessibility": "", "parameterNames": "title", "call": "title: title", "parameters": [CuckooGeneratorFramework.MethodParameter(label: Optional("title"), name: "title", type: "String?", range: CountableRange(792..<806), nameRange: CountableRange(792..<797))], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ClassStubNoReturnFunction"]
     override func setTitle(title: String?)  {
        
            return cuckoo_manager.call("setTitle(title: String?)",
                parameters: (title),
                superclassCall:
                    
                    super.setTitle(title: title)
                    )
        
    }
    
    // ["name": "setImage", "returnSignature": "", "fullyQualifiedName": "setImage()", "parameterSignature": "", "parameterSignatureWithoutNames": "", "inputTypes": "", "isThrowing": false, "isInit": false, "isOverriding": true, "hasClosureParams": false, "@type": "ClassMethod", "accessibility": "", "parameterNames": "", "call": "", "parameters": [], "returnType": "Void", "isOptional": false, "stubFunction": "Cuckoo.ClassStubNoReturnFunction"]
     override func setImage()  {
        
            return cuckoo_manager.call("setImage()",
                parameters: (),
                superclassCall:
                    
                    super.setImage()
                    )
        
    }
    

	struct __StubbingProxy_GnomeDetailsViewController: Cuckoo.StubbingProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	
	    init(manager: Cuckoo.MockManager) {
	        self.cuckoo_manager = manager
	    }
	    
	    var gnomePhotoImageView: Cuckoo.ClassToBeStubbedProperty<MockGnomeDetailsViewController, UIImageView?> {
	        return .init(manager: cuckoo_manager, name: "gnomePhotoImageView")
	    }
	    
	    var infoTableView: Cuckoo.ClassToBeStubbedProperty<MockGnomeDetailsViewController, UITableView?> {
	        return .init(manager: cuckoo_manager, name: "infoTableView")
	    }
	    
	    var presenter: Cuckoo.ClassToBeStubbedProperty<MockGnomeDetailsViewController, GnomeInfoPresenter?> {
	        return .init(manager: cuckoo_manager, name: "presenter")
	    }
	    
	    
	    func viewDidLoad() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockGnomeDetailsViewController.self, method: "viewDidLoad()", parameterMatchers: matchers))
	    }
	    
	    func didReceiveMemoryWarning() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockGnomeDetailsViewController.self, method: "didReceiveMemoryWarning()", parameterMatchers: matchers))
	    }
	    
	    func setTitle<M1: Cuckoo.Matchable>(title: M1) -> Cuckoo.ClassStubNoReturnFunction<(String?)> where M1.MatchedType == String? {
	        let matchers: [Cuckoo.ParameterMatcher<(String?)>] = [wrap(matchable: title) { $0 }]
	        return .init(stub: cuckoo_manager.createStub(for: MockGnomeDetailsViewController.self, method: "setTitle(title: String?)", parameterMatchers: matchers))
	    }
	    
	    func setImage() -> Cuckoo.ClassStubNoReturnFunction<()> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return .init(stub: cuckoo_manager.createStub(for: MockGnomeDetailsViewController.self, method: "setImage()", parameterMatchers: matchers))
	    }
	    
	}

	struct __VerificationProxy_GnomeDetailsViewController: Cuckoo.VerificationProxy {
	    private let cuckoo_manager: Cuckoo.MockManager
	    private let callMatcher: Cuckoo.CallMatcher
	    private let sourceLocation: Cuckoo.SourceLocation
	
	    init(manager: Cuckoo.MockManager, callMatcher: Cuckoo.CallMatcher, sourceLocation: Cuckoo.SourceLocation) {
	        self.cuckoo_manager = manager
	        self.callMatcher = callMatcher
	        self.sourceLocation = sourceLocation
	    }
	
	    
	    var gnomePhotoImageView: Cuckoo.VerifyProperty<UIImageView?> {
	        return .init(manager: cuckoo_manager, name: "gnomePhotoImageView", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    var infoTableView: Cuckoo.VerifyProperty<UITableView?> {
	        return .init(manager: cuckoo_manager, name: "infoTableView", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	    var presenter: Cuckoo.VerifyProperty<GnomeInfoPresenter?> {
	        return .init(manager: cuckoo_manager, name: "presenter", callMatcher: callMatcher, sourceLocation: sourceLocation)
	    }
	    
	
	    
	    @discardableResult
	    func viewDidLoad() -> Cuckoo.__DoNotUse<Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("viewDidLoad()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func didReceiveMemoryWarning() -> Cuckoo.__DoNotUse<Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("didReceiveMemoryWarning()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func setTitle<M1: Cuckoo.Matchable>(title: M1) -> Cuckoo.__DoNotUse<Void> where M1.MatchedType == String? {
	        let matchers: [Cuckoo.ParameterMatcher<(String?)>] = [wrap(matchable: title) { $0 }]
	        return cuckoo_manager.verify("setTitle(title: String?)", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	    @discardableResult
	    func setImage() -> Cuckoo.__DoNotUse<Void> {
	        let matchers: [Cuckoo.ParameterMatcher<Void>] = []
	        return cuckoo_manager.verify("setImage()", callMatcher: callMatcher, parameterMatchers: matchers, sourceLocation: sourceLocation)
	    }
	    
	}

}

 class GnomeDetailsViewControllerStub: GnomeDetailsViewController {
    
     override var gnomePhotoImageView: UIImageView! {
        get {
            return DefaultValueRegistry.defaultValue(for: (UIImageView!).self)
        }
        
        set { }
        
    }
    
     override var infoTableView: UITableView! {
        get {
            return DefaultValueRegistry.defaultValue(for: (UITableView!).self)
        }
        
        set { }
        
    }
    
     override var presenter: GnomeInfoPresenter? {
        get {
            return DefaultValueRegistry.defaultValue(for: (GnomeInfoPresenter?).self)
        }
        
        set { }
        
    }
    

    

    
     override func viewDidLoad()  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
     override func didReceiveMemoryWarning()  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
     override func setTitle(title: String?)  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
     override func setImage()  {
        return DefaultValueRegistry.defaultValue(for: Void.self)
    }
    
}


//
//  GnomeCityPresenter.swift
//  Brastlewark
//
//  Created by Anton Zuev on 7/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//
import Foundation
import AlamofireImage

class GnomeCityPresenter: NSObject {
    let view:AllGnomesViewProtocol!
    let downloader = ImageDownloader()
    let city = GnomeCity()
    
    ///Get information about all citizens of the city
    ///Also you can pass searchTerm to query information and get only part of city citizens
    func getInformationAboutCity(searchTerm:String? = nil) {
        ///ask server about all citizens
        city.getAllInformationAboutCity { (cityInfo) in
            if let gnomeCityInfo = cityInfo
            {
                if let searchTerm = searchTerm,!searchTerm.isEmpty {
                    self.city.habitantsInfo?.Brastlewark = gnomeCityInfo.Brastlewark.filter({ (gnome) -> Bool in
                        return gnome.name.contains(searchTerm)
                    })
                }
                self.view.reloadView()
            }
        }
    }
    
    ///Get number of citizens
    func getNumberOfCitizens()->Int{
        if let count = self.city.habitantsInfo?.Brastlewark.count{
            return count
        } else{
            return 0
        }
    }
    ///It's enough only one section
    func getNumberOfSections()->Int{
        return 1;
    }
    ///Get information about one specific citizen by index
    func getInfoAboutHabitant(index: Int) -> Gnome?{
        return city.habitantsInfo?.Brastlewark[index]
    }
    ///Get the image of the citizen
    func getThumbnailImage(index: Int,completionHandler:@escaping (_:Image)->())
    {
        if let thumbnailUrl = self.getInfoAboutHabitant(index: index)?.thumbnailUrl {
            if let url = URL(string: thumbnailUrl) {
                downloader.download(URLRequest(url: url)) {response in
                    if let image = response.result.value {
                        completionHandler(image)
                    }
                }
            }
        }
    }
    
    init(view:AllGnomesViewProtocol) {
        self.view=view
    }
}

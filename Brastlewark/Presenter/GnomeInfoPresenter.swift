//
//  GnomeInfoPresenter.swift
//  Brastlewark
//
//  Created by Anton Zuev on 8/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation
import AlamofireImage


class GnomeInfoPresenter: NSObject {
    let view:GnomeDetailsProtocol!
    let downloader = ImageDownloader()
    
    var gnomeInfo:Gnome?
    
    ///Set model info of the presenter
    func setGnomeInfo(gnomeInfo:Gnome?) {
        self.gnomeInfo = gnomeInfo
    }
    ///Get number of elements from model for the view
    func getNumberOfRows()->Int{
        if let count = getGnomeInfoDetailsDictionary()?.count{
            return count
        } else{
            return 0
        }
    }
    ///Use only one section
    func getNumberOfSections()->Int{
        return 1;
    }
    ///Get specific title of the information (Age, Height, Weight)
    func getTitleForRow(with index:Int)->String{
        if let dict = self.getGnomeInfoDetailsDictionary() {
            return Array(dict)[index].key
        }
        return ""
    }
    ///Get the value for the specific information by index
    func getDetailInfoTextForRow(with index:Int)->String{
        if let dict = self.getGnomeInfoDetailsDictionary() {
            if let value=Array(dict)[index].value as? String{
                return value
            } else {
                return ""
            }
        }
        return ""
    }
    
    ///Get details information dictionary that contains all information about gnome
    func getGnomeInfoDetailsDictionary() -> [String: Any]?  {
        if let gnomeInfo = self.gnomeInfo {
            var details: [String: Any] = ["Age": String(gnomeInfo.age), "Weight": String(gnomeInfo.weight), "Height":String(gnomeInfo.height), "Hair color": gnomeInfo.hairColor]
            if !gnomeInfo.friends.isEmpty {
                details["Friends"] = gnomeInfo.friends.joined(separator: ", ")
            }
            if !gnomeInfo.professions.isEmpty {
                details["Professions"] = gnomeInfo.professions.joined(separator: ", ")
            }
            return details
        }
        return nil
    }
    
    ///Get image of gnome frome thumbnailUrl link
    func getImage(completionHandler:@escaping (_:Image)->())
    {
        if let thumbnailUrl = gnomeInfo?.thumbnailUrl {
            if let url = URL (string: thumbnailUrl) {
                downloader.download(URLRequest(url: url)) {response in
                    if let image = response.result.value {
                        completionHandler(image)
                    }
                }
            }
        }
    }
    
    ///Show gnome image and name
    func showGnomeImageAndName(){
        self.view.setImage()
        self.view.setTitle(title: self.gnomeInfo?.name)
    }
    
    ///Initialize with view that conforms to GnomeDetailsProtocol
    init(view:GnomeDetailsProtocol) {
        self.view=view
    }
}

//
//  Gnome.swift
//  Brastlewark
//
//  Created by Anton Zuev on 7/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

class Gnome: Codable {
    var idProperty: Int = 0
    var name: String = ""
    var thumbnailUrl: String = ""
    var age: Int = 0
    var weight: Double = 0.0
    var height: Double = 0.0
    var hairColor: String = ""
    var professions: [String] = []
    var friends: [String] = []
    
    enum CodingKeys: String, CodingKey {
        case idProperty = "id"
        case name
        case thumbnailUrl = "thumbnail"
        case age
        case weight
        case height
        case hairColor = "hair_color"
        case professions
        case friends
    }
    
}

//
//  Gnomes.swift
//  Brastlewark
//
//  Created by Anton Zuev on 7/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//


import Alamofire

class GnomeCity: NSObject {
    
    var habitantsInfo: Brastlewark?
    let serverAddress = "https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json"

    //get all information about the city
    func getAllInformationAboutCity(completionHandler: @escaping (_:Brastlewark?)->()){
        Alamofire.request(serverAddress).responseObject { (response: DataResponse<Brastlewark>) in
            if let json = response.value {
                self.habitantsInfo=json
                completionHandler(json)
            }
            else{
                completionHandler(nil)
            }
        }
    }
    
    
    
}

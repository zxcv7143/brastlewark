//
//  ViewController.swift
//  Brastlewark
//
//  Created by Anton Zuev on 7/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import UIKit

protocol AllGnomesViewProtocol {
    func reloadView()
}

protocol AllGnomesViewOutputProtocol {
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}

class AllGnomesViewController: UIViewController, AllGnomesViewProtocol{
    var presenter:GnomeCityPresenter!
    var router:GnomeListRouter!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //configure router
        let router = GnomeListRouter()
        router.viewController = self
        self.router = router
        //configure presenter
        presenter = GnomeCityPresenter(view: self)
        presenter?.getInformationAboutCity()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setup collection view layout to make header stick to the bounds
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadView() {
        self.collectionView.reloadData()
    }
    
}

extension AllGnomesViewController: AllGnomesViewOutputProtocol
{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        router.passDataToNextScene(segue: segue)
    }
}


extension AllGnomesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let searchBarHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView", for: indexPath)
        return searchBarHeader
    }
    
}

extension AllGnomesViewController:  UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if let numberOfSections = self.presenter?.getNumberOfSections() {
            return numberOfSections
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let numberOfRows = self.presenter?.getNumberOfCitizens(){
            return numberOfRows
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionViewCell=self.collectionView.dequeueReusableCell(withReuseIdentifier: "GnomeInfoCell", for: indexPath)
        if let gnomeInfoCell:GnomeInfoCell = collectionViewCell as? GnomeInfoCell{
            gnomeInfoCell.imageView.image = nil
            gnomeInfoCell.nameLabel.text = nil
            
            if let habitant = self.presenter?.getInfoAboutHabitant(index: indexPath.row){
                gnomeInfoCell.nameLabel.text = habitant.name
                self.presenter?.getThumbnailImage(index:indexPath.row,completionHandler: { (image) in
                    //in case of fast scrolling image can be set for other cell that's why we check
                    //the name of the gnome and the image
                    if gnomeInfoCell.nameLabel.text == habitant.name{
                        gnomeInfoCell.imageView.image = image
                    }
                })
                
            }
        }
        return collectionViewCell;
    }
}

extension AllGnomesViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        presenter?.getInformationAboutCity(searchTerm: searchBar.text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        searchBar.showsCancelButton = false
        // Remove focus from the search bar.
        searchBar.endEditing(true)
        //repopulate collection view
        presenter?.getInformationAboutCity()
    }
}


//
//  GnomeInfoViewController.swift
//  Brastlewark
//
//  Created by Anton Zuev on 8/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import UIKit

protocol GnomeDetailsProtocol {
    
    func setTitle(title: String?)
    func setImage()
    
}

class GnomeDetailsViewController: UIViewController, GnomeDetailsProtocol {
    
    @IBOutlet weak var gnomePhotoImageView: UIImageView!
    @IBOutlet weak var infoTableView: UITableView!
    var presenter:GnomeInfoPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.showGnomeImageAndName()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTitle(title: String?) {
        self.title = title
    }
    
    func setImage() {
        self.presenter?.getImage(completionHandler: { (image) in
            self.gnomePhotoImageView.image=image
        })
    }
}

extension GnomeDetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if let numberOfSections = self.presenter?.getNumberOfSections() {
            return numberOfSections
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let numberOfRows = self.presenter?.getNumberOfRows(){
            return numberOfRows
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableViewCell=tableView.dequeueReusableCell(withIdentifier: "DetailTextCell", for: indexPath)
        if let gnomeDetailCell = tableViewCell as? GnomeDetailCell{
            gnomeDetailCell.typeLabel?.text=self.presenter?.getTitleForRow(with: indexPath.row)
            gnomeDetailCell.valueLabel?.text=self.presenter?.getDetailInfoTextForRow(with: indexPath.row)
        }
        return tableViewCell
    }
}

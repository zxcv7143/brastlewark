//
//  SearchBarHeader.swift
//  Brastlewark
//
//  Created by Anton Zuev on 9/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import UIKit

class SearchBarHeader: UICollectionReusableView {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
}

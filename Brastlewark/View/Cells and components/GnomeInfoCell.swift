//
//  GnomeInfoCell.swift
//  Brastlewark
//
//  Created by Anton Zuev on 7/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import UIKit

class GnomeInfoCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}

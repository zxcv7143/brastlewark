//
//  GnomeInfoDetailCellTableViewCell.swift
//  Brastlewark
//
//  Created by Anton Zuev on 9/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import UIKit

class GnomeDetailCell: UITableViewCell {
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

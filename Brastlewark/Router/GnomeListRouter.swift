//
//  GnomeListRouter.swift
//  Brastlewark
//
//  Created by Anton Zuev on 13/6/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation
import UIKit

protocol ListOrdersRouterInput
{
    func navigateToSomewhere()
}

class GnomeListRouter
{
    //define as weak to avoid retain cycle
    weak var viewController: AllGnomesViewController!
    
    // MARK: Navigation
    func navigateToSomewhere()
    {
        // NOTE: Teach the router how to navigate to another scene. Some examples follow:
        
        // 1. Trigger a storyboard segue
        // viewController.performSegueWithIdentifier("ShowSomewhereScene", sender: nil)
        
        // 2. Present another view controller programmatically
        // viewController.presentViewController(someWhereViewController, animated: true, completion: nil)
        
        // 3. Ask the navigation controller to push another view controller onto the stack
        // viewController.navigationController?.pushViewController(someWhereViewController, animated: true)
        
        // 4. Present a view controller from a different storyboard
        // let storyboard = UIStoryboard(name: "OtherThanMain", bundle: nil)
        // let someWhereViewController = storyboard.instantiateInitialViewController() as! SomeWhereViewController
        // viewController.navigationController?.pushViewController(someWhereViewController, animated: true)
    }
    
    ///Pass data to the next scene scene
    func passDataToNextScene(segue: UIStoryboardSegue)
    {
        if segue.identifier == "showDetails" {
            passDataToShowGnomeDetailsScene(segue: segue)
        }
    }
    
    //Define data that will be passed
    func passDataToShowGnomeDetailsScene(segue: UIStoryboardSegue)
    {
        if let gnomeDetailsViewController=segue.destination as? GnomeDetailsViewController
        {
            let gnomeInfoDetailsPresenter=GnomeInfoPresenter(view: gnomeDetailsViewController)
            gnomeDetailsViewController.presenter=gnomeInfoDetailsPresenter
            if let indexPath: IndexPath = self.viewController.collectionView.indexPathsForSelectedItems?.first{
                gnomeInfoDetailsPresenter.setGnomeInfo(gnomeInfo: (self.viewController.presenter?.getInfoAboutHabitant(index: indexPath.row)))
            }
        }
    }
}

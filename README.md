##Small test project **BRASTLEWARK**

In this application we can see information about citizens of one imaginary city of gnomes. There is main view where you can see photos of all gnomes that live in the city.
If you tap on the photo you can see detail information about this gnome. Because there are many gnomes that live in the city you have oppotunity to search for particular one in the search bar above.

- in this project I use very famous library for HTTP networking **Alamofire**
- also **AlamofireImage** library for download and cache images 
- and **AlamofireNetworkActivityIndicator** for control network activity indicator

##Unit Test
 For the mocking of views in the unit tests I use **Cuckoo** library because it's very simple and straightforward to use.


